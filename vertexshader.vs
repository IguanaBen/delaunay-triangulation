#version 130

uniform vec4 mat_scale;

attribute vec3 position;
uniform vec3 color;
uniform int is_point;
uniform mat4 view_matrix;

varying vec3 fragmentColor;
flat out int is_point_out;


void main()
{
	
	is_point_out = is_point;

	gl_PointSize = 4.2;
	vec4 pos;
	pos.xyz = position;
	pos.w = 1.0f;
	gl_Position = view_matrix * pos;

	fragmentColor = color;
}