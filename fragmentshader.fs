#version 130

varying vec3 fragmentColor;
flat in int is_point_out;

void main()
{
	if( is_point_out > 0)
	{
		vec2 coord = gl_PointCoord - vec2(0.5);
		if(length(coord) > 0.5) 
    		discard;
    }

	vec4 color;
	color.xyz = fragmentColor;
	color.w = 1;
	gl_FragColor = color;
}