#include "delaunay.h"


fT ccw(point p1, point p2, point p3)
{
	return (p2.x - p1.x)*(p3.y - p1.y) - (p2.y - p1.y)*(p3.x - p1.x);
}

point::point()
{
	x = 0;
	y = 0;
}

point::point(fT _x, fT _y)
{
	x = _x;
	y = _y;
}

point point::operator +( const point & p)
{
	return point(x + p.x, y + p.y);
}

point point::operator -( const point & p)
{
	return point(x - p.x, y - p.y);
}

point point::operator -()
{
	return point(-x, -y);
}

bool point::operator == (const point &p) const
{
	return (x==p.x && y==p.y);
}


triangle_node::triangle_node(point p1, point p2, point p3)
{
	points[0] = p1;
	points[1] = p2;
	points[2] = p3;

	depth = 0;

	visited = false;

	for(int i=0; i<3; i++)
	{
		neighbours[i] = NULL;
		children[i] = NULL;
	}

}

delaunay_triangulation::delaunay_triangulation(point p1, point p2, point p3)
{
	root = new triangle_node(p1, p2, p3);
	triangle_count = 1;
	structure_nodes_count = 1;
	max_node_depth = 0;


	bounding_box[0].x = -1.0;
	bounding_box[0].y = -1.0;
	bounding_box[1].x = 1.0;
	bounding_box[1].y = 1.0;
}

delaunay_triangulation::delaunay_triangulation(std::vector<point> points)
{	
	// point_v = points;

	// point mx_ypoint = points[0];

	triangle_count = 1;
	structure_nodes_count = 1;
	max_node_depth = 0;

	point_v.clear();
	bounding_box[0].x = points[0].x;
	bounding_box[0].y = points[0].y;
	bounding_box[1].x = points[0].x;
	bounding_box[1].y = points[0].y;

	for(unsigned int i=1; i<points.size(); i++)
	{
		bounding_box[0].x = std::min(bounding_box[0].x, points[i].x);
		bounding_box[0].y = std::min(bounding_box[0].y, points[i].y);

		bounding_box[1].x = std::max(bounding_box[1].x, points[i].x);
		bounding_box[1].y = std::max(bounding_box[1].y, points[i].y);
	}

	double b_width = bounding_box[1].x - bounding_box[0].x;
	double b_height = bounding_box[1].y - bounding_box[0].y;


	root = new triangle_node(
		point( bounding_box[0].x- b_width*100, bounding_box[0].y-1.0),
		point( bounding_box[0].x + b_width*101, bounding_box[0].y-1.0),
		point( bounding_box[0].x + b_width/2, bounding_box[0].y + b_height*100)
	);

	std::vector<point> sh_points = shuffle(points);

	try
	{
		for(unsigned int i=0; i<sh_points.size(); i++)
			add_point(sh_points[i]);
	}
	catch(std::string *s)
	{
		std::cout<<*s<<"\n";
	}

}

delaunay_triangulation::~delaunay_triangulation()
{	
	triangle_count = 1;
	structure_nodes_count = 1;

	std::vector<triangle_node*> to_remove;
	delete_rec(root, to_remove);

	for(unsigned int i=0; i<to_remove.size(); i++)
		delete to_remove[i];
	point_v.clear();
}

void delaunay_triangulation::reset(point p1, point p2, point p3)
{	
	triangle_count = 1;
	structure_nodes_count = 1;
	max_node_depth = 0;


	std::vector<triangle_node*> to_remove;
	delete_rec(root, to_remove);

	for(unsigned int i=0; i<to_remove.size(); i++)
		delete to_remove[i];

	point_v.clear();
	root = new triangle_node(p1, p2, p3);

	// bounding_box[0].x = -1.0;
	// bounding_box[0].y = -1.0;
	// bounding_box[1].x = 1.0;
	// bounding_box[1].y = 1.0;
}

void delaunay_triangulation::reset()
{	
	triangle_count = 1;
	structure_nodes_count = 1;
	max_node_depth = 0;

	point p1 = root->points[0];
	point p2 = root->points[1];
	point p3 = root->points[2];


	std::vector<triangle_node*> to_remove;
	delete_rec(root, to_remove);

	for(unsigned int i=0; i<to_remove.size(); i++)
		delete to_remove[i];

	point_v.clear();
	root = new triangle_node(p1, p2, p3);

	// bounding_box[0].x = -1.0;
	// bounding_box[0].y = -1.0;
	// bounding_box[1].x = 1.0;
	// bounding_box[1].y = 1.0;
}

std::vector<point> delaunay_triangulation::shuffle(std::vector<point> vec)
{	
	srand (time(NULL));
	for(unsigned int i = 0; i < vec.size(); i++)
	{
	        int j = rand() % (i+1);
	        std::swap(vec[i], vec[j]);
	}

	return vec;
}

bool delaunay_triangulation::in_triangle(point p, triangle_node tri)
{
	fT r[3];
	r[0] =  ccw(tri.points[0], tri.points[1], p );
	r[1] =  ccw(tri.points[1], tri.points[2], p );
	r[2] =  ccw(tri.points[2], tri.points[0], p );

	for(int i=0; i<3; i++)
		if(r[i] < 0)
			return false;

	return true;
}

bool delaunay_triangulation::on_triangle_corner(point p, triangle_node tri)
{	
	fT r[3];
	r[0] =  ccw(tri.points[0], tri.points[1], p );
	r[1] =  ccw(tri.points[1], tri.points[2], p );
	r[2] =  ccw(tri.points[2], tri.points[0], p );

	int c0 = 0;
	for(int i=0; i<3; i++)
		c0 += (r[i] == 0);

	return (c0 > 1);
}

bool delaunay_triangulation::on_triangle_edge(point p, triangle_node tri)
{	
	fT r[3];
	r[0] =  ccw(tri.points[0], tri.points[1], p );
	r[1] =  ccw(tri.points[1], tri.points[2], p );
	r[2] =  ccw(tri.points[2], tri.points[0], p );

	int c0 = 0;
	for(int i=0; i<3; i++)
		c0 += (r[i] == 0);

	return (c0 > 0);
}

void delaunay_triangulation::add_point(point p)
{

	// std::cout<<ccw(root->points[0], root->points[1], p )<<"\n";

	if(!in_triangle(p, *root))
		throw new std::string("point outside structure");

	triangle_node *tri = find_triangle(root, p);


	if(on_triangle_corner(p, *tri))
		return;

	point_v.push_back(p);

	if(on_triangle_edge(p, *tri))
	{
		// return;
		// std::cout<<"EDGE\n";

		triangle_node *neighbour_node = NULL;

		for(int i=0; i<3; i++)
			if(tri->neighbours[i] != NULL)
				if(on_triangle_edge(p, *tri->neighbours[i]))
					neighbour_node = tri->neighbours[i];

		if(neighbour_node == NULL)
			throw new std::string("point outside structure");

		int neighbour_edge1;
		int neighbour_edge2;

		for(int i = 0; i<3; i++)
		{
			if(tri->neighbours[i] == neighbour_node)
				neighbour_edge1 = i;

			if(neighbour_node->neighbours[i] == tri)
				neighbour_edge2 = i;
		}

		int id_t1 = (neighbour_edge1+2) % 3;	//point not on edge
		int id_t2 = (neighbour_edge2+2) % 3;

		max_node_depth = std::max(max_node_depth, tri->depth + 1);
		max_node_depth = std::max(max_node_depth, neighbour_node->depth + 1);

		tri->children[0] = 
			new triangle_node(p, tri->points[(id_t1 + 2) % 3], tri->points[id_t1]);

		tri->children[1] = 
			new triangle_node(p, tri->points[id_t1], tri->points[(id_t1 + 1) % 3]);

		neighbour_node->children[0] =
			new triangle_node(p, neighbour_node->points[(id_t2 + 2) % 3], 
				neighbour_node->points[id_t2]);

		neighbour_node->children[1] =
			new triangle_node(p, neighbour_node->points[id_t2], 
				neighbour_node->points[(id_t2 + 1) % 3]);


		tri->children[0]->depth  = tri->depth + 1;
		tri->children[1]->depth  = tri->depth + 1;
		neighbour_node->children[0]->depth  = neighbour_node->depth + 1;
		neighbour_node->children[1]->depth  = neighbour_node->depth + 1;


		tri->children[0]->neighbours[1] = tri->neighbours[(id_t1 + 2) % 3];
		tri->children[1]->neighbours[1] = tri->neighbours[id_t1];

		neighbour_node->children[0]->neighbours[1] = neighbour_node->neighbours[(id_t2 + 2) % 3];
		neighbour_node->children[1]->neighbours[1] = neighbour_node->neighbours[id_t2];

		tri->children[0]->neighbours[0] = neighbour_node->children[1];
		tri->children[0]->neighbours[2] = tri->children[1];

		tri->children[1]->neighbours[0] = tri->children[0];
		tri->children[1]->neighbours[2] = neighbour_node->children[0];

		neighbour_node->children[0]->neighbours[0] = tri->children[1];
		neighbour_node->children[0]->neighbours[2] = neighbour_node->children[1];

		neighbour_node->children[1]->neighbours[0] = neighbour_node->children[0];
		neighbour_node->children[1]->neighbours[2] = tri->children[0];

		if(tri->neighbours[(id_t1 + 2 )% 3] != NULL)
			for(int j=0; j<3; j++)
				if(tri->neighbours[(id_t1 + 2 )% 3]->neighbours[j] == tri)
					tri->neighbours[(id_t1 + 2 )% 3]->neighbours[j] = tri->children[0];

		if(tri->neighbours[id_t1] != NULL)
			for(int j=0; j<3; j++)
				if(tri->neighbours[id_t1]->neighbours[j] == tri)
					tri->neighbours[id_t1]->neighbours[j] = tri->children[1];

		if(neighbour_node->neighbours[(id_t2 + 2 )% 3] != NULL)
			for(int j=0; j<3; j++)
				if(neighbour_node->neighbours[(id_t2 + 2 )% 3]->neighbours[j] == neighbour_node)
					neighbour_node->neighbours[(id_t2 + 2 )% 3]->neighbours[j] = neighbour_node->children[0];

		if(neighbour_node->neighbours[id_t2] != NULL)
			for(int j=0; j<3; j++)
				if(neighbour_node->neighbours[id_t2]->neighbours[j] == neighbour_node)
					neighbour_node->neighbours[id_t2]->neighbours[j] = neighbour_node->children[1];

		for(int i=0; i<2; i++)
			legalize_edge(tri->children[i], tri->children[i]->neighbours[1]);

		for(int i=0; i<2; i++)
			legalize_edge(neighbour_node->children[i], neighbour_node->children[i]->neighbours[1]);

		triangle_count += 2;

		structure_nodes_count += 4;

	}
	else
	{
		max_node_depth = std::max(max_node_depth, tri->depth + 1);

		for(int i=0; i<3; i++)
		{
			tri->children[i] = new triangle_node(tri->points[i], tri->points[(i+1) % 3], p );
			tri->children[i]->neighbours[0] = tri->neighbours[i];
			tri->children[i]->depth = tri->depth + 1;
		}

		for(int i=0; i<3; i++)
			if(tri->neighbours[i] != NULL)
			{
				for(int j=0; j<3; j++)
					if(tri->neighbours[i]->neighbours[j] == tri)
						tri->neighbours[i]->neighbours[j] = tri->children[i];
			}

		for(int i=0; i<3; i++)
			tri->children[i]->neighbours[1] = tri->children[(i+1) % 3];

		for(int i=0; i<3; i++)
			tri->children[i]->neighbours[2] = tri->children[(i+2) % 3];

		triangle_count += 2;
		structure_nodes_count += 3;

		// for(int i=0; i<3; i++)
		// 	std::cout<<tri->children[i]<<" | "
		// 		<<tri->children[i]->neighbours[0]<<" "<<
		// 		tri->children[i]->neighbours[1]<<" "<<
		// 		tri->children[i]->neighbours[2]<<"\n";

		for(int i=0; i<3; i++)
			legalize_edge(tri->children[i], tri->children[i]->neighbours[0]);

	}

}

triangle_node* delaunay_triangulation::find_triangle(triangle_node *t_node, point p)
{

	for(int i=0; i<3; i++)
		if(t_node->children[i] != NULL)
			if(in_triangle(p, *t_node->children[i] ))
				return find_triangle(t_node->children[i], p);

	return t_node;
}

void delaunay_triangulation::get_triangle_data(float *data, int &num_triangles)
{
	int data_offset = 0;
	get_triangle_data_rec(root, data, data_offset);
	// std::cout<<data_offset<<"\n";
	num_triangles = data_offset/9;
	// std::cout<<num_triangles<<"\n";
	flip_visited(root);
}

void delaunay_triangulation::get_data(float *vertices, int &num_vertices,
								float *data, int &num_triangles)
{

	num_vertices = point_v.size();


	for(unsigned int i=0; i< point_v.size(); i++)
	{
		vertices[3*i] = point_v[i].x;
		vertices[3*i+1] = point_v[i].y;
		vertices[3*i+2] = 0;
	}

	int data_offset = 0;
	get_triangle_data_rec(root, data, data_offset);
	// std::cout<<data_offset<<"\n";
	num_triangles = data_offset/9;
	// std::cout<<num_triangles<<"\n";
	flip_visited(root);
}

void delaunay_triangulation::get_triangle_data_rec(triangle_node *t_node, 
												float *data, int &data_offset)
{
	t_node->visited = true;

	int num_children = 0;
	int num_neighbours = 0;

	// int same_as_root = 0; //number of points equal to
					      //bounding triangle

	for(int i=0; i<3; i++)
	{
		if(t_node->children[i] != NULL)	
		{
			num_children++;
			if(!t_node->children[i]->visited)
				get_triangle_data_rec(t_node->children[i], data, data_offset);
		}
	}

	for(int i=0; i<3; i++)
		for(int j=0; j<3; j++)
			if(t_node->points[i] == root->points[j])
				return;

	if(num_children == 0)
	{
		for(int i=0; i<3; i++)
		{
			data[data_offset++] = t_node->points[i].x;
			data[data_offset++] = t_node->points[i].y;
			data[data_offset++] = 0;
		}
	}
}

void delaunay_triangulation::delete_rec(triangle_node *t_node, 
										std::vector<triangle_node*> &to_remove )
{
	t_node->visited = true;

	for(int i=0; i<3; i++)
		if(t_node->children[i] != NULL)
			if(!t_node->children[i]->visited)
				delete_rec(t_node->children[i], to_remove);

	to_remove.push_back(t_node);
}


void delaunay_triangulation::flip_visited(triangle_node *t_node)
{
	t_node->visited = false;

	for(int i=0; i<3; i++)
		if(t_node->children[i] != NULL)	
			if(t_node->children[i]->visited)
				flip_visited(t_node->children[i]);
}


int delaunay_triangulation::get_triangle_count()
{
	return triangle_count;
}

fT delaunay_triangulation::get_angle(point vec1, point vec2)
{
	fT dot = vec1.x*vec2.x + vec1.y*vec2.y;
	fT len1 = vec1.x*vec1.x + vec1.y*vec1.y;
	fT len2 = vec2.x*vec2.x + vec2.y*vec2.y;

	return glm::acos(dot/glm::sqrt(len1 * len2));
}

bool delaunay_triangulation::is_illegal(triangle_node *t1, triangle_node *t2)
{
	int neighbour_edge1;
	int neighbour_edge2;

	fT newAngleEdge1 = 0;
	fT newAngleEdge2 = 0;

	for(int i = 0; i<3; i++)
	{
		if(t1->neighbours[i] == t2)
			neighbour_edge1 = i;

		if(t2->neighbours[i] == t1)
			neighbour_edge2 = i;
	}

	point edge_end1 = t1->points[(neighbour_edge1 + 0) % 3];
	point edge_end2 = t1->points[(neighbour_edge1 + 1) % 3];

	point not_on_edge_t1 = t1->points[(neighbour_edge1+2) % 3];
	point not_on_edge_t2 = t2->points[(neighbour_edge2+2) % 3];

	fT alpha1 = 
		get_angle(not_on_edge_t1 - edge_end1, edge_end2 - edge_end1); //angle near edge
	fT alpha2 = 
		get_angle(not_on_edge_t2 - edge_end1, edge_end2 - edge_end1);//angle adjacent ot alpha1

	fT beta1 = 
		get_angle(not_on_edge_t1 - edge_end2, edge_end1 - edge_end2); //angle near edge
	fT beta2 = 
		get_angle(not_on_edge_t2 - edge_end2, edge_end1 - edge_end2); //angle adjacent ot beta1


	fT min_noflip = std::min(
			get_angle(edge_end1 - not_on_edge_t1 , edge_end2 - not_on_edge_t1),
			get_angle(edge_end1 - not_on_edge_t2 , edge_end2 - not_on_edge_t2) );

	min_noflip = std::min(min_noflip, alpha1);
	min_noflip = std::min(min_noflip, alpha2);
	min_noflip = std::min(min_noflip, beta1);
	min_noflip = std::min(min_noflip, beta2);

	fT min_flip = alpha1 + alpha2;

	min_flip = std::min(min_flip, beta1 + beta2);

	min_flip = std::min(min_flip, 
			get_angle(not_on_edge_t2 - not_on_edge_t1, edge_end1 - not_on_edge_t1) );

	min_flip = std::min(min_flip, 
			get_angle(not_on_edge_t2 - not_on_edge_t1, edge_end2 - not_on_edge_t1) );

	min_flip = std::min(min_flip, 
			get_angle(not_on_edge_t1 - not_on_edge_t2, edge_end1 - not_on_edge_t2) );

	min_flip = std::min(min_flip, 
			get_angle(not_on_edge_t1 - not_on_edge_t2, edge_end2 - not_on_edge_t2) );

	return (min_flip > min_noflip);
}

bool delaunay_triangulation::is_illegal_mat(triangle_node *t1, triangle_node *t2)
{
	float matrix_data[16];

	matrix_data[0] = t1->points[0].x;
	matrix_data[1] = t1->points[0].y;
	matrix_data[2] = t1->points[0].x*t1->points[0].x + t1->points[0].y*t1->points[0].y;
	matrix_data[3] = 1;

	matrix_data[4] = t1->points[1].x;
	matrix_data[5] = t1->points[1].y;
	matrix_data[6] = t1->points[1].x*t1->points[1].x + t1->points[1].y*t1->points[1].y;
	matrix_data[7] = 1;

	matrix_data[8] = t1->points[2].x;
	matrix_data[9] = t1->points[2].y;
	matrix_data[10] = t1->points[2].x*t1->points[2].x + t1->points[2].y*t1->points[2].y;
	matrix_data[11] = 1;

	point q;

	for(int i = 0; i<3; i++)
		if(t2->neighbours[i] == t1)
			q = t2->points[(i+2) % 3];

	matrix_data[12] = q.x;
	matrix_data[13] = q.y;
	matrix_data[14] = q.x*q.x + q.y*q.y;
	matrix_data[15] = 1;

	glm::mat4 matrix = glm::make_mat4(matrix_data);
	// std::cout<<"D: "<<glm::determinant(matrix)<<"\n";
	return (glm::determinant(matrix) > 0.0);

}

void delaunay_triangulation::legalize_edge(triangle_node *t1, triangle_node *t2)
{	
	if(t1 == NULL || t2 == NULL)
		return;


	int neighbour_edge1 = 0;
	int neighbour_edge2 = 0;

	for(int i = 0; i<3; i++)
	{
		if(t1->neighbours[i] == t2)
			neighbour_edge1 = i;

		if(t2->neighbours[i] == t1)
			neighbour_edge2 = i;
	}

	int id_t1 = (neighbour_edge1+2) % 3;	//point not on edge
	int id_t2 = (neighbour_edge2+2) % 3;

	if(is_illegal_mat(t1, t2))
	{
		max_node_depth = std::max(max_node_depth, t1->depth + 1);
		max_node_depth = std::max(max_node_depth, t2->depth + 1);

		// std::cout<<"illegal\n\n";

		// triangle_node *n_t1 = new triangle_node(point(-0.5, -0.5), 
			// point(-0.5, 0.5) , point(-0.3, -0.5) );

		// triangle_node *n_t2 = new triangle_node(point(-0.5, -0.5), 
			// point(-0.5, 0.5) , point(-0.3, -0.5) );


		triangle_node *n_t1 = new triangle_node(t1->points[id_t1], 
									t1->points[(id_t1+1) % 3], t2->points[id_t2]);
		
		triangle_node *n_t2 = new triangle_node(t2->points[id_t2], 
									t2->points[(id_t2+1) % 3], t1->points[id_t1]);

		// std::cout<<t1->points[id_t1].x<<","<<t1->points[id_t1].y
		// 	<<"|   |"<<t1->points[(id_t1+1) % 3].x<<","<<t1->points[(id_t1+1) % 3].y<<
		// 	"|   |"<<t2->points[id_t2].x<<","<<t2->points[id_t2].y<<"\n";

		// std::cout<<t2->points[id_t2].x<<","<<t2->points[id_t2].y
		// 	<<"|   |"<<t2->points[(id_t2+1) % 3].x<<","<<t2->points[(id_t2+1) % 3].y<<
		// 	"|   |"<<t1->points[id_t1].x<<","<<t1->points[id_t1].y<<"\n";
		// std::cout<<t2->points[id_t2].x<<" "<<t2->points[(id_t2+1) % 3]<<" "<<t1->points[id_t1]<<"\n\n";


		n_t1->neighbours[0] = t1->neighbours[id_t1];
		n_t1->neighbours[1] = t2->neighbours[(id_t2 + 2) % 3];
		n_t1->neighbours[2] = n_t2;

		n_t1->depth = std::max(t1->depth, t2->depth) + 1;
		n_t2->depth = std::max(t1->depth, t2->depth) + 1;



		for(int j=0; j<3; j++)
		{
			
			if(t1->neighbours[id_t1] != NULL)
				if(t1->neighbours[id_t1]->neighbours[j] == t1)
					t1->neighbours[id_t1]->neighbours[j] = n_t1;
			
			if(t2->neighbours[(id_t2 + 2) % 3] != NULL)
				if(t2->neighbours[(id_t2 + 2) % 3]->neighbours[j] == t2)
					t2->neighbours[(id_t2 + 2) % 3]->neighbours[j] = n_t1;	
		}

		n_t2->neighbours[0] = t2->neighbours[id_t2];
		n_t2->neighbours[1] = t1->neighbours[(id_t1 + 2) % 3];
		n_t2->neighbours[2] = n_t1;

		for(int j=0; j<3; j++)
		{
			if(t2->neighbours[id_t2] != NULL)
				if(t2->neighbours[id_t2]->neighbours[j] == t2)
					t2->neighbours[id_t2]->neighbours[j] = n_t2;
			
			if(t1->neighbours[(id_t1 + 2) % 3] != NULL)
				if(t1->neighbours[(id_t1 + 2) % 3]->neighbours[j] == t1)
					t1->neighbours[(id_t1 + 2) % 3]->neighbours[j] = n_t2;	
		}

		t1->children[0] = n_t1;
		t1->children[1] = n_t2;

		t2->children[0] = n_t1;
		t2->children[1] = n_t2;

		legalize_edge(n_t1, n_t1->neighbours[1]);
		legalize_edge(n_t2, n_t2->neighbours[0]);

		structure_nodes_count += 2;

	}
}
 

void delaunay_triangulation::get_bounding_points(point &p1, point &p2)
{
	p1 = bounding_box[0];
	p2 = bounding_box[1];
}

int delaunay_triangulation::get_structure_nodes_count()
{
	return structure_nodes_count;
}


int delaunay_triangulation::get_max_nodes_depth()
{
	return max_node_depth;
}
