#version 130

attribute vec3 position;
attribute vec2 vertexUV;

varying vec2 UV;


void main(){

	gl_Position = vec4(position,1);
	
	UV = vertexUV;
}
