#ifndef _SHADER_LOADER_H_
#define _SHADER_LOADER_H_

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include <iostream>
#include <vector>
#include <fstream>

GLuint LoadShaders(const char * vertex_file_path, const char * fragment_file_path);

#endif