#ifndef _RENDERER_H_
#define _RENDERER_H_

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp> 
#include <glm/gtx/transform.hpp>
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>

#include "text_renderer.h"
#include "shader_loader.h"
#include "delaunay.h"

class renderer
{
	private:
		GLFWwindow* window;
		delaunay_triangulation *del_tri;
		static void handle_mouse_click(GLFWwindow * window, int button, 
								int action, int mods);

		static void handle_mouse_click_static(GLFWwindow * window, int button, 
								int action, int mods);

		static void handle_mouse_move(GLFWwindow * window, double _x, double _y);

		static void handle_mouse_scroll(GLFWwindow * window, double _x, double _y);

		void handle_keyboard_press();


		std::string intToString(int x);
		std::string floatToString(float x);

		int _width;
		int _height;
		bool right_mouse_pressed;
		bool stats_button_state;
		GLfloat zoom_ratio;
		GLfloat offsetX;
		GLfloat offsetY;

	public:
		renderer();
		~renderer();
		void init_window(int width, int height, delaunay_triangulation *_del_tri);
		void start_render_interactive();
		void start_render_static(float gen_time);

		void begin_draw();
		void end_draw();
};



#endif