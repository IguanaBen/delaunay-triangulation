#ifndef _DELAUNAY_H_
#define _DELAUNAY_H_

#include <stddef.h>
#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <algorithm>
#include <vector>
#include <time.h>

#define fT double

struct point
{
	fT x, y;

	point(fT _x, fT _y);
	point();

	point operator +( const point & p);
	point operator -( const point & p);
	point operator -();
	bool operator == (const point &p) const;

};

fT ccw(point p1, point p2, point p3);

struct triangle_node
{

	triangle_node(point p1, point p2, point p3);

	bool visited;  //used for DFS

	point points[3];			  // coords, CCW
	triangle_node *neighbours[3]; // (0,1), (1,2), (2,0)
	triangle_node *children[3];

	int depth;
};


class delaunay_triangulation
{
	public:
		delaunay_triangulation(point p1, point p2, point p3); // must be ccw
		delaunay_triangulation(std::vector<point> points);
		~delaunay_triangulation();


		int get_triangle_count();
		void get_triangle_data(float *data, int &num_triangles); //3*triangle_count points
		void get_data(float *vertices, int &num_vertices,
								float *data, int &num_triangles); // vertices 3*triangle_count points

		void add_point(point p);
		void reset(point p1, point p2, point p3);
		void reset();


		void get_bounding_points(point &p1, point &p2);

		int get_structure_nodes_count();
		int get_max_nodes_depth();


	private:
		triangle_node *root;
		std::vector<point> point_v;

		int triangle_count;
		int structure_nodes_count;

		int max_node_depth;

		triangle_node *find_triangle(triangle_node *t_node, point p);
		bool in_triangle(point p, triangle_node tri);
		bool on_triangle_corner(point p, triangle_node tri);
		bool on_triangle_edge(point p, triangle_node tri);
		void get_triangle_data_rec(triangle_node *t_node, float *data, int &data_offset);
		void flip_visited(triangle_node *t_node);
		bool is_illegal(triangle_node *t1, triangle_node *t2);		//edge between t1 and t2
		bool is_illegal_mat(triangle_node *t1, triangle_node *t2);		//edge between t1 and t2

		void legalize_edge(triangle_node *t1, triangle_node *t2);	//t1 - triangle containing p

		void delete_rec(triangle_node *t1, std::vector<triangle_node*> &to_remove );

		fT get_angle(point vec1, point vec2);

		std::vector<point> shuffle(std::vector<point> vec);

		point bounding_box[2];	//left lower corner, right upper corner


};


#endif