#version 130

varying vec2 UV;
uniform sampler2D textSampler;

void main()
{	
	vec4 col = texture2D( textSampler, UV );

	if(col.x < 0.05 && col.y < 0.05 && col.z < 0.05)
		discard;

	gl_FragColor = texture2D( textSampler, UV );
}