NAME = delaunay_demo
CPP = g++ -g -W
LINK =  -lglfw3 -lGL -lGLU -lglew32 -lGL -lX11 -lXxf86vm -lpthread -lXrandr -lXi
INCLUDE = -I include
LIB = -L lib


delaunay_demo: delaunay.o renderer.o demo.cpp
	$(CPP) demo.cpp delaunay.o renderer.o text_renderer.o texture_loader.o shader_loader.o -o $(NAME) $(INCLUDE) $(LIB) $(LINK)

renderer.o: shader_loader.o text_renderer.o renderer.cpp renderer.h
	$(CPP) -c renderer.cpp $(INCLUDE)

delaunay.o: delaunay.cpp delaunay.h
	$(CPP) -c delaunay.cpp $(INCLUDE)

text_renderer.o: shader_loader.o texture_loader.o text_renderer.cpp text_renderer.h
	$(CPP) -c text_renderer.cpp $(INCLUDE)

shader_loader.o: shader_loader.cpp shader_loader.h
	$(CPP) -c shader_loader.cpp $(INCLUDE)
 
texture_loader.o: texture_loader.cpp texture_loader.h
	$(CPP) -c texture_loader.cpp $(INCLUDE)

clean:
	rm -rf *.o 
distclean:
	rm -rf *.o $(NAME)
