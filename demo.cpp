#include <iostream>
#include <fstream>
#include <stdio.h>
#include <ctime>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "delaunay.h"
#include "renderer.h"

using namespace std;

int main(int argc, char *argv[])
{
  
  renderer r;

  if(argc == 1)
  {
    delaunay_triangulation del_tri(point(-10004.0f, -10000.0f), 
                                  point(10000.0f, -10000.0f), 
                                  point(10000.0f, 10000.0f)
                                );
    r.init_window(1000, 600, &del_tri);
    r.start_render_interactive();

  }
  else
  {

    ifstream file;
    file.open(argv[1]);

    if(!file.is_open())
    {
      cout<<"error opening: "<<argv[1]<<"\n";
      return 1;
    }

    vector<point> points;

    int n;
    file>>n;
    for(int i=0; i<n; i++)  
    {
      double x,y;
      file>>x>>y;
      points.push_back(point(x,y));
    }
    clock_t timer_start = clock();

    delaunay_triangulation del_tri(points);

    clock_t timer_end = clock();

    // std::cout<< double(timer_end - timer_start) / CLOCKS_PER_SEC<<"\n";

    r.init_window(1000, 600, &del_tri);

    if(argc == 3)
      if(argv[2][0] == 'e')
        r.start_render_interactive();

    if(argc == 2)
      r.start_render_static(double(timer_end - timer_start) / CLOCKS_PER_SEC);


  }

  return 0; 
}