#include "renderer.h"

renderer::renderer()
{

}

renderer::~renderer()
{
	glfwTerminate();
}


void renderer::init_window(int width, int height, delaunay_triangulation *_del_tri)
{
	// return 0;

	_width = width;
	_height = height;


	stats_button_state = false;
	right_mouse_pressed = false;

	zoom_ratio = 1.0f;

	del_tri = _del_tri;

	if( !glfwInit() )
		throw new std::string("glfw init error");

	glfwWindowHint(GLFW_SAMPLES, 16);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);

	window = glfwCreateWindow( width, height, "Delaunay Demo", NULL, NULL);
	if( window == NULL )
	{
		glfwTerminate();
		throw new std::string("create window error");
	}

	glfwMakeContextCurrent(window);


	if (glewInit() != GLEW_OK)
	{
		throw new std::string("glew init error");
	}

	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	glfwSetWindowUserPointer(window, this);	
	// glfwSetCursorPosCallback(window, handle_mouse_click);
}

void renderer::begin_draw()
{
	glClear( GL_COLOR_BUFFER_BIT );
}

void renderer::end_draw()
{
	glfwSwapBuffers(window);
}

void renderer::handle_mouse_click(GLFWwindow * window, int button, 
								int action, int mods)
{

	// static int added = 0;
	// static const float floatX[] = {0.7, 0.5};
		// static const float floatY[] = {-0.7, -0.5};

	// static bool right_mouse_pressed = false;
	renderer *r = (renderer*)glfwGetWindowUserPointer(window);

	if(button == GLFW_MOUSE_BUTTON_2  && action == GLFW_PRESS)
		r->right_mouse_pressed = true;

	if(button == GLFW_MOUSE_BUTTON_2  && action == GLFW_RELEASE)
		r->right_mouse_pressed = false;

	if(button == GLFW_MOUSE_BUTTON_1  && action == GLFW_PRESS)
	{

		int width = r->_width;
		int height = r->_height;
		double x, y;
		glfwGetCursorPos(window, &x, &y);
		double px  = x - width/2.0;
		double py =  y - height/2.0;
		px = 2.0*px/width;
		py = -2.0*py/height;

		try
		{	
			if( px >= -0.9 && px <= -0.70  && py >= -0.9 && py <= -0.83)
			{
				r->stats_button_state = !r->stats_button_state;
			}
			else
				if( px >= -0.61 && px <= -0.40  && py >= -0.9 && py <= -0.83)
				{
					// r->del_tri->reset(point(-10004.0, -10000.0), 
	    //                               point(10000.0, -10000.0), 
	    //                               point(10000.0, 10000.0)
	    //                             );
					r->del_tri->reset();
					r->zoom_ratio = 1.0f;
					r->offsetX = 0.0f;
					r->offsetY = 0.0f;
				}
				else
				{


				    point left_lower_bp, rigth_upper_bp;
					r->del_tri->get_bounding_points(left_lower_bp, rigth_upper_bp);

					float transformX = left_lower_bp.x + 
							(rigth_upper_bp.x - left_lower_bp.x)/2.0;

					float transformY = left_lower_bp.y +
						(rigth_upper_bp.y - left_lower_bp.y)/2.0;

					float scaleX = 2.0/(rigth_upper_bp.x - left_lower_bp.x);
					float scaleY = 2.0/(rigth_upper_bp.y - left_lower_bp.y);
					scaleX = scaleX*0.85;
					scaleY = scaleY*0.85;

					// r->del_tri->add_point( point((px )/ r->zoom_ratio - r->offsetX, 
					// 		(py ) / r->zoom_ratio - r->offsetY));

					px = px/r->zoom_ratio - r->offsetX;
					py = py/r->zoom_ratio - r->offsetY;

					px = px/scaleX + transformX;
					py = py/scaleY + transformY;

					r->del_tri->add_point(point(px, py));
				}

		}
		catch(std::string *s)
		{
			// std::cout<<*s<<"\n";
		}
		// std::cout<< px <<" "<< py <<" "<< r->del_tri->get_triangle_count()<<"\n";
	}
}

void renderer::handle_mouse_click_static(GLFWwindow * window, int button, 
								int action, int mods)
{	
	if(button == GLFW_MOUSE_BUTTON_1  && action == GLFW_PRESS)
	{
		renderer *r = (renderer*)glfwGetWindowUserPointer(window);
		int width = r->_width;
		int height = r->_height;
		double x, y;
		glfwGetCursorPos(window, &x, &y);
		long double px  = x - width/2;
		long double py =  y - height/2;
		px = 2*px/width;
		py = -2*py/height;
		
		if( px >= -0.9 && px <= -0.70  && py >= -0.9 && py <= -0.83)
		{
			r->stats_button_state = !r->stats_button_state;
		}
	}
}

void renderer::handle_mouse_move(GLFWwindow * window, double _x, double _y)
{
	renderer *r = (renderer*)glfwGetWindowUserPointer(window);

	if(r->right_mouse_pressed)
	{
		int width = r->_width;
		int height = r->_height;
		// double x, y;
		// glfwGetCursorPos(window, &x, &y);
		static double last_x = 0.0;
		static double last_y = 0.0;

		double px  = _x - width/2;
		double py =  _y - height/2;

		px = 2*px/width;
		py = (-2)*py/height;

		
			
		try
		{	
			
			point left_lower_bp, rigth_upper_bp;
			r->del_tri->get_bounding_points(left_lower_bp, rigth_upper_bp);

			float transformX = left_lower_bp.x + 
						(rigth_upper_bp.x - left_lower_bp.x)/2.0;

			float transformY = left_lower_bp.y +
					(rigth_upper_bp.y - left_lower_bp.y)/2.0;

			float scaleX = 2.0/(rigth_upper_bp.x - left_lower_bp.x);
			float scaleY = 2.0/(rigth_upper_bp.y - left_lower_bp.y);
			scaleX = scaleX*0.85;
			scaleY = scaleY*0.85;

					// r->del_tri->add_point( point((px )/ r->zoom_ratio - r->offsetX, 
					// 		(py ) / r->zoom_ratio - r->offsetY));
			px = px/r->zoom_ratio - r->offsetX;
			py = py/r->zoom_ratio - r->offsetY;

			px = px/scaleX + transformX;
			py = py/scaleY + transformY;

			double dist = glm::sqrt((last_x-px)*(last_x-px) + (last_y-py)*(last_y-py));

			if(dist > (rigth_upper_bp.x - left_lower_bp.x)/40.0)
			{
				r->del_tri->add_point(point(px, py));

				last_x = px;
				last_y = py;
			}
		}
		catch(std::string *s)
		{
			// std::cout<<*s<<"\n";
		}
	}
}

void renderer::handle_mouse_scroll(GLFWwindow * window, double _x, double _y)
{
	renderer *r = (renderer*)glfwGetWindowUserPointer(window);
	// std::cout<<_y<<"\n";	
	if(_y > 0)
		r->zoom_ratio += 0.05;
	else
		r->zoom_ratio -= 0.05;

	if(r->zoom_ratio > 5.0)
		r->zoom_ratio = 5.0;

	if(r->zoom_ratio < 1.0)
		r->zoom_ratio = 1.0;
}

void renderer::handle_keyboard_press()
{
	// renderer *r = (renderer*)glfwGetWindowUserPointer(window);

	if(glfwGetKey(window, GLFW_KEY_UP ) == GLFW_PRESS)
	{
		offsetY += 0.01/zoom_ratio;
		if(offsetY > 1.0)
			offsetY = 1.0;
	}


	if(glfwGetKey(window, GLFW_KEY_DOWN ) == GLFW_PRESS)
	{
		offsetY -= 0.01/zoom_ratio;
		if(offsetY < -1.0)
			offsetY = -1.0;
	}


	if(glfwGetKey(window, GLFW_KEY_RIGHT ) == GLFW_PRESS)
	{
		offsetX += 0.01/zoom_ratio;
		if(offsetX > 1.0)
			offsetX = 1.0;
	}


	if(glfwGetKey(window, GLFW_KEY_LEFT ) == GLFW_PRESS)
	{
		offsetX -= 0.01/zoom_ratio;
		if(offsetX < -1.0)
			offsetX = -1.0;
	}
}

void renderer::start_render_interactive()
{

    GLuint ProgramID = LoadShaders("vertexshader.vs", "fragmentshader.fs");

    GLuint position_loc =  glGetAttribLocation(ProgramID, "position");
    GLuint color_loc =  glGetUniformLocation(ProgramID, "color");
    GLuint is_point_loc =  glGetUniformLocation(ProgramID, "is_point");
    GLuint view_matrix_loc =  glGetUniformLocation(ProgramID, "view_matrix");

	glfwSetMouseButtonCallback(window, handle_mouse_click);
	glfwSetCursorPosCallback(window, handle_mouse_move);
	glfwSetScrollCallback(window, handle_mouse_scroll);


	GLfloat *vertices = new GLfloat[1000000];
	GLfloat *tri_vertices = new GLfloat[1000000];


	GLuint vertex_buffer;
	glGenBuffers(1, &vertex_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);

    // glEnableVertexAttribArray(position_loc);

    // glm::mat4 myScalingMatrix = glm::scale(0.95f, 0.95f, 0.95f);
    glm::vec3 edge_color(1.0, 0.0, 0.0);
    glm::vec3 fill_color(0.0, 0.0, 1.0);
    glm::vec3 point_color(0.0, 1.0, 0.0);

    glm::vec3 magenta(1.0, 0.0, 0.56);
    glm::vec3 green(0.0, 0.8, 0.0);



    double d = glm::acos(0.3);

    text_renderer text_r;



    // gluPerspective(45.0f,(float)_width/(float)_height,0.1f,100.0f);

    const GLfloat button1verts[] =
    {
    	-0.9, -0.9, 0,
    	-0.70, -0.9, 0,
    	-0.70, -0.83, 0,

    	-0.70, -0.83, 0,
    	-0.9, -0.83, 0,
    	-0.9, -0.9, 0,
    };

   const GLfloat button2verts[] =
    {
    	-0.61, -0.9, 0,
    	-0.40, -0.9, 0,
    	-0.40, -0.83, 0,

    	-0.40, -0.83, 0,
    	-0.61, -0.83, 0,
    	-0.61, -0.9, 0,
    };

    point left_lower_bp, rigth_upper_bp;
	del_tri->get_bounding_points(left_lower_bp, rigth_upper_bp);

	float transformX = left_lower_bp.x + 
			(rigth_upper_bp.x - left_lower_bp.x)/2.0;

	float transformY = left_lower_bp.y +
		(rigth_upper_bp.y - left_lower_bp.y)/2.0;

	float scaleX = 2.0/(rigth_upper_bp.x - left_lower_bp.x);
	float scaleY = 2.0/(rigth_upper_bp.y - left_lower_bp.y);


	glm::mat4 transform = glm::translate(-transformX, -transformY, 1.0f);
	glm::mat4 scale = glm::scale(scaleX*0.85f, scaleY*0.85f, 1.0f);

	do
	{

		glm::mat4 transform2 = glm::translate(offsetX, offsetY, 0.0f);
    	glm::mat4 scale2 = glm::scale(zoom_ratio, zoom_ratio, 1.0f);
		glm::mat4 view_matrix  = scale2*transform2*scale*transform;

		int n_verts, n_tri;
		del_tri->get_data(vertices, n_verts, tri_vertices, n_tri);

		glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 9* n_tri, tri_vertices, GL_STREAM_DRAW);

		glClear(GL_COLOR_BUFFER_BIT);

        glPolygonMode(GL_FRONT, GL_FILL);


		glUseProgram(ProgramID);

		glUniformMatrix4fv(view_matrix_loc, 1, GL_FALSE, &view_matrix[0][0]);

		glUniform1i(is_point_loc, 0);

		glUniform3fv(color_loc, 1, glm::value_ptr(fill_color));

    	glEnableVertexAttribArray(is_point_loc);
		
    	glEnableVertexAttribArray(position_loc);
        glVertexAttribPointer( position_loc, 3, GL_FLOAT, GL_FALSE, 0,  (void*)0);
        
        glDrawArrays(GL_TRIANGLES, 0, n_tri * 3);

        
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glLineWidth(1.0);

		glEnable (GL_LINE_SMOOTH);
		glEnable (GL_BLEND);
		glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glHint (GL_LINE_SMOOTH_HINT, GL_NICEST);	

		glUniform3fv(color_loc, 1, glm::value_ptr(edge_color));

        glDrawArrays(GL_TRIANGLES, 0, n_tri * 3);

		glUniform3fv(color_loc, 1, glm::value_ptr(point_color));

		glEnable(GL_PROGRAM_POINT_SIZE);
		// glPointSize(1.0);

		glUniform1i(is_point_loc, 1);

		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 3* n_verts, vertices, GL_STREAM_DRAW);
        glDrawArrays(GL_POINTS, 0, n_verts);

        glPolygonMode(GL_FRONT, GL_FILL);

        view_matrix = glm::mat4();
		glUniformMatrix4fv(view_matrix_loc, 1, GL_FALSE, &view_matrix[0][0]);


		glUniform1i(is_point_loc, 0);
		glUniform3fv(color_loc, 1, glm::value_ptr(green));
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 18, button2verts, GL_STREAM_DRAW);
        glDrawArrays(GL_TRIANGLES, 0, 6);

        if(!stats_button_state)
			glUniform3fv(color_loc, 1, glm::value_ptr(magenta));


		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 18, button1verts, GL_STREAM_DRAW);
        glDrawArrays(GL_TRIANGLES, 0, 6);
		// glUniform3fv(color_loc, 1, glm::value_ptr(fill_color));
        glDisableVertexAttribArray(is_point_loc);
        glDisableVertexAttribArray(position_loc);

        text_r.draw("STATS" , -0.9, -0.9, 0.05, (_width/_height)*0.8);
        text_r.draw("RESET" , -0.6, -0.9, 0.05, (_width/_height)*0.8);

        if(stats_button_state)
        {
        	text_r.draw("Vertices:" + intToString(n_verts), 
        		0.2, 0.8, 0.05, (_width/_height)*0.7);

        	text_r.draw("Structure nodes:" + intToString(del_tri->get_structure_nodes_count()), 
        		0.2, 0.7, 0.05, (_width/_height)*0.7);

        	text_r.draw("Triangles:" + intToString(n_tri), 
        		0.2, 0.6, 0.05, (_width/_height)*0.7);

        	text_r.draw("Max node depth:" + intToString(del_tri->get_max_nodes_depth()), 
        		0.2, 0.5, 0.05, (_width/_height)*0.7);
        }

		glfwSwapBuffers(window);

		handle_keyboard_press();
		glfwPollEvents();
	}while(glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
		   glfwWindowShouldClose(window) == 0 );

	delete[] vertices;
	delete[] tri_vertices;
}

void renderer::start_render_static(float gen_time)
{
	glfwSetMouseButtonCallback(window, handle_mouse_click_static);
	glfwSetScrollCallback(window, handle_mouse_scroll);

    GLuint ProgramID = LoadShaders("vertexshader.vs", "fragmentshader.fs");

    GLuint position_loc =  glGetAttribLocation(ProgramID, "position");
    GLuint color_loc =  glGetUniformLocation(ProgramID, "color");
    GLuint is_point_loc =  glGetUniformLocation(ProgramID, "is_point");
    GLuint view_matrix_loc =  glGetUniformLocation(ProgramID, "view_matrix");

	// glfwSetMouseButtonCallback(window, handle_mouse_click);

	GLfloat *vertices = new GLfloat[3000000];
	GLfloat *tri_vertices = new GLfloat[3000000];


	GLuint vertex_buffer;
	glGenBuffers(1, &vertex_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);

    // glEnableVertexAttribArray(position_loc);

    // glm::mat4 myScalingMatrix = glm::scale(0.95f, 0.95f, 0.95f);
    glm::vec3 edge_color(1.0, 0.0, 0.0);
    glm::vec3 fill_color(0.0, 0.0, 1.0);
    glm::vec3 point_color(0.0, 1.0, 0.0);

	int n_verts, n_tri;
	del_tri->get_data(vertices, n_verts, tri_vertices, n_tri);

	point left_lower_bp, rigth_upper_bp;
	del_tri->get_bounding_points(left_lower_bp, rigth_upper_bp);

	float transformX = left_lower_bp.x + 
			(rigth_upper_bp.x - left_lower_bp.x)/2.0;

	float transformY = left_lower_bp.y +
		(rigth_upper_bp.y - left_lower_bp.y)/2.0;
	// float transformY = 0.0f;

	float scaleX = 2.0/(rigth_upper_bp.x - left_lower_bp.x);
	float scaleY = 2.0/(rigth_upper_bp.y - left_lower_bp.y);


	glm::mat4 transform = glm::translate(-transformX, -transformY, 1.0f);
	glm::mat4 scale = glm::scale(scaleX*0.85f, scaleY*0.85f, 1.0f);

	const GLfloat button1verts[] =
    {
    	-0.9, -0.9, 0,
    	-0.70, -0.9, 0,
    	-0.70, -0.83, 0,

    	-0.70, -0.83, 0,
    	-0.9, -0.83, 0,
    	-0.9, -0.9, 0,
    };


    glm::vec3 green(0.0, 0.8, 0.0);
    glm::vec3 magenta(1.0, 0.0, 0.56);

    text_renderer text_r;

    stats_button_state = true;

	do
	{
		glm::mat4 transform2 = glm::translate(offsetX, offsetY, 0.0f);
    	glm::mat4 scale2 = glm::scale(zoom_ratio, zoom_ratio, 1.0f);
		glm::mat4 view_matrix  = scale2*transform2*scale*transform;

		glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 9* n_tri, tri_vertices, GL_STATIC_DRAW);

		glClear(GL_COLOR_BUFFER_BIT);

        glPolygonMode(GL_FRONT, GL_FILL);

		glUseProgram(ProgramID);

		glUniformMatrix4fv(view_matrix_loc, 1, GL_FALSE, &view_matrix[0][0]);

		glUniform1i(is_point_loc, 0);

		glUniform3fv(color_loc, 1, glm::value_ptr(fill_color));

    	glEnableVertexAttribArray(is_point_loc);
		
    	glEnableVertexAttribArray(position_loc);
        glVertexAttribPointer( position_loc, 3, GL_FLOAT, GL_FALSE, 0,  (void*)0);
        
        glDrawArrays(GL_TRIANGLES, 0, n_tri * 3);

        
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glLineWidth(1.0);

		glEnable (GL_LINE_SMOOTH);
		glEnable (GL_BLEND);
		glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glHint (GL_LINE_SMOOTH_HINT, GL_NICEST);	

		glUniform3fv(color_loc, 1, glm::value_ptr(edge_color));

        glDrawArrays(GL_TRIANGLES, 0, n_tri * 3);

		glUniform3fv(color_loc, 1, glm::value_ptr(point_color));

		glEnable(GL_PROGRAM_POINT_SIZE);

		glUniform1i(is_point_loc, 1);

		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 3* n_verts, vertices, GL_STATIC_DRAW);
        glDrawArrays(GL_POINTS, 0, n_verts);

        view_matrix = glm::mat4();
		glUniformMatrix4fv(view_matrix_loc, 1, GL_FALSE, &view_matrix[0][0]);

		glUniform1i(is_point_loc, 0);

        glPolygonMode(GL_FRONT, GL_FILL);

        if(!stats_button_state)
			glUniform3fv(color_loc, 1, glm::value_ptr(magenta));
		else
			glUniform3fv(color_loc, 1, glm::value_ptr(green));

		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 18, button1verts, GL_STATIC_DRAW);
        glDrawArrays(GL_TRIANGLES, 0, 6);
        glDisableVertexAttribArray(is_point_loc);
        glDisableVertexAttribArray(position_loc);

        text_r.draw("STATS" , -0.9, -0.9, 0.05, (_width/_height)*0.8);

        if(stats_button_state)
        {
        	text_r.draw("Vertices:" + intToString(n_verts), 
        		0.2, 0.8, 0.05, (_width/_height)*0.7);

        	text_r.draw("Structure nodes:" + intToString(del_tri->get_structure_nodes_count()), 
        		0.2, 0.7, 0.05, (_width/_height)*0.7);

        	text_r.draw("Max node depth:" + intToString(del_tri->get_max_nodes_depth()), 
        		0.2, 0.6, 0.05, (_width/_height)*0.7);

        	text_r.draw("Triangles:" + intToString(n_tri), 
        		0.2, 0.5, 0.05, (_width/_height)*0.7);

        	text_r.draw("Generated in:" + floatToString(gen_time), 
        		0.2, 0.4, 0.05, (_width/_height)*0.7);
        }


		glfwSwapBuffers(window);

		handle_keyboard_press();
		glfwPollEvents();
	}while(glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
		   glfwWindowShouldClose(window) == 0 );

	delete[] vertices;
	delete[] tri_vertices;
}

std::string renderer::intToString(int x)
{
   std::stringstream ss;
   ss << x;
   return ss.str();
}

std::string renderer::floatToString(float x)
{
   std::stringstream ss;
   ss << x;
   return ss.str();
}

